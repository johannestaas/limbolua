-- store
--  db:auth_user(name,pass)
--  db:new_user(name,pass)
require('store')

Guy = {}
Guy.__index = Guy

NEWB = {
    last_x=512,
    last_y=512,
    hp=100,
    xhp=100,
    mp=100,
    xmp=100,
    level=1,
}

Guy._new = function(name)
    g = {}
    setmetatable(g, Guy)
    g.name = name
    return g
end

Guy.new = function(name, pass)
    g = Guy._new(name)
    if db:user_exists(name) then
        -- return false if can't auth
        if not db:auth_user(name, pass) then return nil end
        -- good pass
        usertable = db:get_data(name)
        if usertable == nil then return nil end
        for k,v in pairs(usertable) do
            g[k] = v
        end
    else
        for k,v in pairs(NEWB) do
            g[k] = v
        end
        g.x = g.last_x
        g.y = g.last_y
        -- create new user
        db:new_user(name, pass, g)
    end
    return g
end
