require 'ai'

Ent = {}
Ent.__index = Ent
function Ent.new(name)
    e = {name=name}
    return e
end


Act = {}
Act.__index = Act
setmetatable(Act, Ent)

function Act.new(name)
    a = Ent.new(name)
    setmetatable(a, Act)
    return a
end


