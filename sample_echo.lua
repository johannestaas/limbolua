#!/usr/bin/env lua
-- usage: <sample_lua.lua> <PORT>
socket = require('socket')
port = arg[1]
server = assert(socket.bind("*", port))
print("Serving on "..port)
data = nil
while data ~= 'q' do
    client = server:accept()
    client:settimeout(5)
    while data ~= 'q' do
        data,err = client:receive()
        if not err then client:send('gotit!\n') end
        if data == 'q' then 
            client:send('quitting!\n') 
            client:close()
        end
    end
end
