
AI_STATE = {
    idle = 'idle',
    active = 'active',
    erratic = 'erratic',
}

AI = {}
AI.__index = AI

function AI.new()
    transform = {
        idle = 10,
        active = 80,
        erratic = 10,
    }
    ai = {
        state=AI_STATE.idle,
        duration=10,
        transform=transform,
    }
    setmetatable(ai, AI)
    return ai
end

