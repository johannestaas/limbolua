-- This will basically be the "area" data that is sent to the client
-- What's around, layout of the map, etc.

Tile = {}
Tile.__index = Tile

-- args = coords
Tile.new = function(x,y)
    t = {}
    setmetatable(t, Tile)
    -- x,y coords
    t.x = x
    t.y = y
    -- the type of terrain (0=abyss?)
    t.typ = 0
    return t
end

Map = {}
Map.__index = Map

-- size in constructor
Map.new = function(w,h)
    m = {}
    setmetatable(m, Map)
    -- width and height
    m.w = w
    m.h = h
    for x=1,w do
        col = {}
        for y=1,h do
            col[y] = Tile.new(x,y)
        end
        m[x] = col
    end
    return m
end

function Map:show(x,y)
    for y=y,y+20 do
        for x=x,x+78 do
            io.write(self[x][y].typ)
        end
        io.write('\n')
    end
end
