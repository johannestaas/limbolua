-- some lua sql for database storage
sql = require('luasql.sqlite3')
require('md5')
require('json')

-- http://www.keplerproject.org/luasql/manual.html
env = sql.sqlite3()
db_path = 'limbo.db'

do
    local f = io.open(db_path,"r")
    if f~=nil then 
        io.close(f) 
        _G.conn = env:connect(db_path)
    else 
        _G.conn = env:connect(db_path)
        _G.conn:execute(
        [[
            create table Users (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT UNIQUE,
                pass TEXT,
                data TEXT
            );
        ]])
        _G.conn:commit()
    end
    _G.db = {conn=conn}
end


-- (shameless copy from interweb)
-- Backslash-escape special characters:
function addslashes(s)
  s = string.gsub(s, "(['\"\\])", "\\%1")
  return (string.gsub(s, "%z", "\\0"))
end

-- creates a database for users
function db:create()
    return self.conn:commit()
end

-- returns true if user already exists
function db:user_exists(name)
    name = addslashes(name)
    x = self.conn:execute(
        "select name from Users where name is '"..name.."';")
    assert(x~=nil)
    if x:fetch() ~= nil then return true end
    return false
end

-- inserts name and hashed pass into the database
-- returns true if successful, false if already exists
function db:new_user(name, pass, usertable)
    if self:user_exists(name) then return false end
    name = addslashes(name)
    pass = addslashes(pass)
    salt = 'rockindonkey'
    hash = md5.sumhexa(salt..pass)
    -- store usertable as json
    -- if usertable ever takes input from the user, SANITIZE!
    data = json.encode(usertable)
    self.conn:execute(
        "insert into Users (name,pass,data) "..
        "values ('"..name.."','"
                   ..hash.."','"
                   ..data.."');") 
    self.conn:commit()
    return true
end

-- returns true if they are valid creds, false otherwise
function db:auth_user(name, pass)
    name = addslashes(name)
    pass = addslashes(pass)
    salt = 'rockindonkey'
    hash = md5.sumhexa(salt..pass)
    x = self.conn:execute(
        "select pass from Users where name is '"..name.."';")
    db_hash = x:fetch()
    return (db_hash == hash)
end

-- return usertable from json data field
function db:get_data(name)
    name = addslashes(name)
    x = self.conn:execute(
        "select data from Users where name is '"..name.."';")
    data = x:fetch()
    usertable = json.decode(data)
    return usertable
end
